from django.contrib import admin
from .models import Reaction, Announcement

# Register your models here.
class ReactionAdmin(admin.ModelAdmin):
    model = Reaction
    extra = 1

class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'pub_datetime', 'body')
    search_fields = ('title', 'author', 'body')
    list_filter = ('author', 'pub_datetime')


admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)
from django.apps import AppConfig


class AnnouncementBoardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Announcement_Board'

from django.db import models
from django.urls import reverse
from dashboard.models import WidgetUser

# Create your models here.

class Announcement(models.Model):
    title = models.CharField(max_length = 50)
    body = models.TextField()
    author = models.ForeignKey(
        WidgetUser, 
        on_delete = models.CASCADE
    )
    pub_datetime = models.DateTimeField()

class Reaction(models.Model):
    name = models.CharField(max_length = 50)
    tally = models.IntegerField()
    announcement = models.ForeignKey(
        Announcement,
        on_delete = models.CASCADE
    )


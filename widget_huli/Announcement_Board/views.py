from django.shortcuts import render
from django.http import HttpResponse

from .models import Announcement, Reaction

# Create your views here.

def index(request):
    return_string = '<body> <ul>'
    reaction_string = ''

    for announcement in Announcement.objects.all():
        announcement_string = '<li>{} by {} published <br>{}</li>'.format(
            announcement.title, announcement.author, announcement.pub_datetime.strftime('%m/%d/%Y %H:%M %p'),
            announcement.body
        )
        for reaction in Reaction.objects.all():
            if reaction.announcement == announcement:
                reaction_string += '<li>{}: {}</li>'.format(reaction.name, reaction.tally
        )
        
        return_string += announcement_string
        return_string += reaction_string
        return_string += '<br>'

    return_string += '</ul></body>'

    html_string = '<htmk>{}</html>'.format(return_string)

    return HttpResponse(html_string)
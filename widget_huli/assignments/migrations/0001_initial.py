# Generated by Django 3.0 on 2023-03-05 12:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=10)),
                ('title', models.CharField(max_length=255)),
                ('section', models.CharField(blank=True, max_length=3)),
            ],
        ),
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True)),
                ('perfect_score', models.IntegerField(blank=True, default=0)),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assignments.Course')),
            ],
        ),
    ]

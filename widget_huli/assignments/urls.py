from django.urls import path

from .views import assignments
from .views import AssignmentDetails, AssignmentAdd, AssignmentEdit


urlpatterns = [
    path('', assignments, name='assignments'),
    path('<pk>/details', AssignmentDetails.as_view(), name='assignment-details'),
    path('add', AssignmentAdd.as_view(), name='assignment-add'),
    path('<pk>/edit', AssignmentEdit.as_view(), name='assignment-edit'),
]

# This might be needed depending on your Django version
app_name = "assignments"

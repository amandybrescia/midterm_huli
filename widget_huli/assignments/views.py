from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Assignment, Course


def assignments(request):
    context = {}
    context["assignment_list"] = Assignment.objects.all()
    return render(request, 'assignments/assignments.html', context)

class AssignmentDetails(DetailView):
    model = Assignment
    template_name = 'assignments/assignment-details.html'

class AssignmentAdd(CreateView):
    model = Assignment
    template_name = 'assignments/assignment-add.html'
    fields = [
        'name', 
        'description', 
        'course', 
        'perfect_score',]

class AssignmentEdit(UpdateView):
    model = Assignment
    template_name = 'assignments/assignment-edit.html'
    fields = [
        'name',
        'description',
        'course',
        'perfect_score',
    ]

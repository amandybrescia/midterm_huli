from django.urls import path
from .views import dashboard_view, WidgetUserView, WidgetUserCreateView, WidgetUserUpdateView

urlpatterns = [
    path('',dashboard_view, name = 'dashboard'),
    path('/widgetusers/<pk>/details', WidgetUserView.as_view(), name='widgetuser-details'),
    path('/widgetusers/add', WidgetUserCreateView.as_view(), name='widgetuser-add'),
    path('/widgetusers/<pk>/edit', WidgetUserUpdateView.as_view(), name='widgetuser-edit'),
]

app_name = "dashboard"
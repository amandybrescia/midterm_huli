from django.contrib import admin
from .models import ForumPost, Reply

# Register your models here.

class ReplyInLine(admin.TabularInline):
    model = Reply
    extra = 1
    
class ForumPostAdmin(admin.ModelAdmin):
    inlines = [ReplyInLine,]
    
    list_display = ('author', 'title', 'pub_datetime', 'body')
    
    search_fields = ('title', 'author', 'body')
    
    list_filter = ('author', 'pub_datetime')
    

admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply)
from django.urls import path
from . import views
from .views import (ForumViews, ForumPostDetails, 
                    ForumPostNew, ForumPostEdit)

urlpatterns = [
    path('', views.ForumViews, name='forum'),
    path('forumposts/<int:pk>/details/', ForumPostDetails.as_view(), name="forumpostdetails"),
    path('forumposts/add/', ForumPostNew.as_view(), name='forumpostnew'),
    path('forumposts/<int:pk>/edit/', ForumPostEdit.as_view(), name = 'forumpostedit'),
]

app_name = "forum"
